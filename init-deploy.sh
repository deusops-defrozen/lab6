#!/bin/bash

if [ ! "$(docker volume ls -q -f name="pg-data")" ]; then
    docker volume create pg-data
fi

if [ ! "$(docker network ls -q -f name=frontend)" ]; then
    docker network create frontend
fi

if [ ! "$(docker network ls -q -f name=backend)" ]; then
    docker network create backend
fi

if [ ! "$(docker ps -q -f name=postgres)" ]; then
    docker run -d --name postgres --env-file ./.env --network backend -v pg-data:/var/lib/postgresql/data -v ./db/imports/init.sql:/docker-entrypoint-initdb.d/init.sql postgres:16.2-alpine3.19
else
    echo "Контейнер с именем postgres уже запущен."
fi
